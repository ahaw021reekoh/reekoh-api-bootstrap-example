function getReekohDevices(){

  HTTP_BODY = ""
  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {

      json = JSON.parse(this.responseText)

      if(this.status === 200){
       $("#debug_log").append(`Response Code: ${this.status} \n`)
       $("#debug_log").append(`Response Body: ${JSON.stringify(json,null,4)} \n`)
       for(device in json["data"]){
         $("#devices-table tbody").append("<tr>")
         $("#devices-table tbody").append(`<td> ${json["data"][device]["_id"]} </td>`)
         $("#devices-table tbody").append(`<td> ${json["data"][device]["name"]} </td>`)
         $("#devices-table tbody").append(`<td> ${json["data"][device]["group"]} </td>`)
         $("#devices-table tbody").append(`<td> ${Object.keys(json["data"][device]["metadata"])} </td>`)
         $("#devices-table tbody").append(`<td> ${moment(json["data"][device]["createdDate"]).format("DD-MMM-YYYY")} </td>`)
         $("#devices-table tbody").append(`<td> ${moment(json["data"][device]["updatedDate"]).format("DD-MMM-YYYY")}  </td>`)
         $("#devices-table tbody").append(`<td> <a href="https://api.reekoh.io${json["data"][device]["_links"][0]["href"]}"> Update </a> <a href="https://api.reekoh.io${json["data"][device]["_links"][0]["href"]}">  Delete </a> </td>`)
         $("#devices-table tbody").append("</tr>")
     }//for loop
     }//if loop

      else{
        $("#debug_log").append(`Response Code: ${this.status} \n`)
        $("#debug_log").append(`Response Body :${JSON.stringify(json,null,4)} \n`)
       }


    }
  });

  $("#debug_log").append("Sending HTTP GET request to https://api.reekoh.io/devices \n")
  $("#debug_log").append(`HTTP Request Header - Authorization: Bearer ${TOKEN.slice(40,60)}  (Token Shortened) \n`)
  $("#debug_log").append(`HTTP Request Header - Content-Type: application/json \n`)
  $("#debug_log").append(`HTTP Payload: ${HTTP_BODY} \n`)


  xhr.open("GET", "https://api.reekoh.io/devices");
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.setRequestHeader("Authorization", `Bearer ${TOKEN}`);
  xhr.send(HTTP_BODY);


}//function
