var TOKEN = ""
var TOKEN_TYPE = "Bearer"
var TOKEN_EXPIRY = ""
var HTTP_BODY = ""
var URL = "https://api.reekoh.io"

function authenticate(){

HTTP_BODY = JSON.stringify({
  "email": $("#reekoh_email").val(),
  "password": $("#reekoh_password").val()
});

var debug_data = JSON.stringify({
  "email": $("#reekoh_email").val(),
  "password": "****************"
});

var xhr = new XMLHttpRequest();

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === 4) {

    json = JSON.parse(this.responseText)

    if(this.status === 200){
     TOKEN = json["idToken"]
     TOKEN_EXPIRY = json["expiry"]
     $("#auth_status_icon").html(` <div class="alert alert-success">  Authentication Succeeded.  </div>   `)
     $("#auth_result").html(`<p><b>Token Expires:</b> ${moment(json["expiry"]).tz("Australia/Sydney").format("HH:mm DD-MMM-YYYY")} </p>`)
     $("#debug_log").append(`Response Code: ${this.status} \n`)
     $("#debug_log").append(`Response Body: ${JSON.stringify(json,null,4)} \n`)
     $("#debug_log").append(`Characters 40-60 of Token: ${TOKEN.slice(40,60)} \n`)
     tokenTicker()
    }

    else{
      $("#auth_result").html(`<b>API Response:</b> ${json["message"]}`)
      $("#auth_status_icon").html(`  <div class="alert alert-danger">  Authentication Failed.  </div>  `)
      $("#debug_log").append(`Response Code: ${this.status} \n`)
      $("#debug_log").append(`Response Body :${JSON.stringify(json,null,4)} \n`)
     }


  }
});

$("#debug_log").append("Sending POST data to https://api.reekoh.io/users/auth \n")
$("#debug_log").append(`Data Payload:  ${debug_data} \n`)

xhr.open("POST", "https://api.reekoh.io/users/auth");
xhr.setRequestHeader("Content-Type", "application/json");
xhr.send(HTTP_BODY);

}//authenticate

function tokenTicker(){

    var expires = moment.utc(TOKEN_EXPIRY)

}
